module git.lubar.me/ben/engine

go 1.16

require (
	github.com/BenLubar/opus v0.0.0-20210627075520-3ea609ce74a4
	github.com/chai2010/webp v1.1.0
	golang.org/x/exp v0.0.0-20210625193404-fa9d1d177d71
	golang.org/x/mobile v0.0.0-20210614202936-7c8f154d1008
)

replace golang.org/x/mobile => git.lubar.me/ben/mobile v0.0.0-20210628181033-31562d479066
