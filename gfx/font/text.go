package font

import (
	"unicode/utf8"

	"git.lubar.me/ben/engine/gfx"
)

func (f *Font) Width(text string) float32 {
	var width float32

	var prev rune

	for i, letter := range text {
		if i != 0 {
			width += f.Advance(prev, letter)
		}

		prev = letter
	}

	if len(text) != 0 {
		width += f.Advance(prev, 0)
	}

	return width
}

func (f *Font) DrawBorder(b *gfx.Batch, text string, x, y, z, sx, sy float32, centered bool, tint, borderTint gfx.Color) {
	b.Reserve(2 * utf8.RuneCountInString(text))

	if centered {
		x -= f.Width(text) * sx / 2
	}

	for _, tintFlags := range [2]struct {
		tint  gfx.Color
		flags gfx.RenderFlags
	}{
		{borderTint, gfx.FlagNoDiscard | gfx.FlagMTSDF | gfx.FlagType2},
		{tint, gfx.FlagNoDiscard | gfx.FlagMTSDF},
	} {
		x0 := x

		var prev rune

		for i, letter := range text {
			if i != 0 {
				x0 += f.Advance(prev, letter) * sx
			}

			prev = letter

			if g := f.Glyph(letter); g != nil {
				b.AppendEx(g, x0, y, z, sx, sy, tintFlags.tint, tintFlags.flags, 0, 0, 0)
			}
		}
	}
}
