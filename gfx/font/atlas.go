package font

// Atlas is a structure that holds the JSON exported by msdf-atlas-gen.
type Atlas struct {
	Meta AtlasMeta `json:"atlas"`
	Variant
}

type MultiAtlas struct {
	Meta     AtlasMeta `json:"atlas"`
	Variants []Variant `json:"variants"`
}

type Variant struct {
	Name    string        `json:"name,omitempty"`
	Metrics Metrics       `json:"metrics"`
	Glyphs  []Glyph       `json:"glyphs"`
	Kerning []KerningPair `json:"kerning"`
}

// AtlasMeta holds data about the generated texture sheet.
type AtlasMeta struct {
	// Type is the atlas mask type. It must be "mtsdf".
	Type string `json:"type"`
	// DistanceRange is number of pixels over which the signed distance
	// field goes from fully opaque to fully transparent.
	DistanceRange float32 `json:"distanceRange"`
	// Size is the number of pixels in the sprite sheet per EM unit.
	Size float32 `json:"size"`
	// Width is the width of the sprite sheet in pixels.
	// It should be a power of 2.
	Width int `json:"width"`
	// Height is the height of the sprite sheet in pixels.
	// It should be a power of 2.
	Height int `json:"height"`
	// YOrigin is the side of the image that corresponds to y = 0.
	// It must be "bottom".
	YOrigin string `json:"yOrigin"`
}

// Metrics holds data about the font itself.
type Metrics struct {
	EmSize             float32 `json:"emSize"`
	LineHeight         float32 `json:"lineHeight"`
	Ascender           float32 `json:"ascender"`
	Descender          float32 `json:"descender"`
	UnderlineY         float32 `json:"underlineY"`
	UnderlineThickness float32 `json:"underlineThickness"`
}

// Glyph holds data about one glyph in the font atlas.
type Glyph struct {
	Unicode rune    `json:"unicode"`
	Advance float32 `json:"advance"`
	Plane   *Bounds `json:"planeBounds,omitempty"`
	Atlas   *Bounds `json:"atlasBounds,omitempty"`
}

// Bounds is the bounds of a rectangle.
type Bounds struct {
	Left   float32 `json:"left"`
	Bottom float32 `json:"bottom"`
	Right  float32 `json:"right"`
	Top    float32 `json:"top"`
}

// KerningPair holds data about the distance between specific characters
// when they are rendered next to each other.
type KerningPair struct {
	Unicode1 rune    `json:"unicode1"`
	Unicode2 rune    `json:"unicode2"`
	Advance  float32 `json:"advance"`
}
