package font

import (
	"git.lubar.me/ben/engine/gfx"
	"git.lubar.me/ben/engine/internal"
)

type Font struct {
	Metrics      Metrics
	asciiGlyph   [128]*gfx.Sprite
	asciiAdvance [128]float32
	glyphs       map[rune]*gfx.Sprite
	advance      map[rune]float32
	kerning      map[[2]rune]float32
}

// NewFont prepares a font atlas for rendering.
//
// These files are generated using msdf-atlas gen, which is available from
// https://github.com/Chlumsky/msdf-atlas-gen
//
// An example of a command to generate an atlas:
//
//     msdf-atlas-gen -font /usr/share/fonts/truetype/dejavu/DejaVuSans.ttf \
//         -fontname "DejaVu Sans" -charset charset.txt -emrange 0.2 \
//         -potr -type mtsdf -format png -imageout text.png -json text.json
func New(sheet *gfx.Sheet, atlas *Atlas) *Font {
	f := &Font{
		Metrics: atlas.Metrics,
		glyphs:  make(map[rune]*gfx.Sprite, len(atlas.Glyphs)),
		advance: make(map[rune]float32, len(atlas.Glyphs)),
		kerning: make(map[[2]rune]float32, len(atlas.Kerning)),
	}

	widthScale := 1.0 / float32(atlas.Meta.Width)
	heightScale := 1.0 / float32(atlas.Meta.Height)

	for _, g := range atlas.Glyphs {
		f.advance[g.Unicode] = g.Advance

		if g.Atlas == nil {
			continue
		}

		s := &gfx.Sprite{
			Named: internal.Name(sheet.Name() + ":" + atlas.Name + " glyph '" + string(g.Unicode) + "'"),
			Sheet: sheet,

			X0: g.Plane.Left,
			X1: g.Plane.Right,
			Y0: g.Plane.Top,
			Y1: g.Plane.Bottom,
			S0: g.Atlas.Left * widthScale,
			S1: g.Atlas.Right * widthScale,
			T0: 1 - g.Atlas.Bottom*heightScale,
			T1: 1 - g.Atlas.Top*heightScale,
		}

		f.glyphs[g.Unicode] = s
	}

	for _, k := range atlas.Kerning {
		f.kerning[[2]rune{k.Unicode1, k.Unicode2}] = k.Advance
	}

	for i := range f.asciiAdvance {
		ch := rune(i)

		a, ok := f.advance[ch]
		if !ok {
			ch = ' '
			a = f.advance[ch]
		}

		f.asciiGlyph[i] = f.glyphs[ch]
		f.asciiAdvance[i] = a
	}

	return f
}

// NewFonts prepares multiple font atlases for rendering.
//
// The input to this function is what msdf-atlas-gen creates when passed the
// `-and` argument.
//
// See NewFont for an example of a command that generates a single-font atlas.
func NewMulti(sheet *gfx.Sheet, atlas *MultiAtlas) []*Font {
	fonts := make([]*Font, len(atlas.Variants))

	for i := range fonts {
		fonts[i] = New(sheet, &Atlas{
			Meta:    atlas.Meta,
			Variant: atlas.Variants[i],
		})
	}

	return fonts
}

func (f *Font) Advance(letter, next rune) float32 {
	k := f.kerning[[2]rune{letter, next}]

	if letter < 128 {
		return f.asciiAdvance[letter] + k
	}

	if a, ok := f.advance[letter]; ok {
		return a + k
	}

	return f.asciiAdvance[' ']
}

func (f *Font) Glyph(letter rune) *gfx.Sprite {
	if letter < 128 {
		return f.asciiGlyph[letter]
	}

	if g, ok := f.glyphs[letter]; ok {
		return g
	}

	return f.asciiGlyph[' ']
}
