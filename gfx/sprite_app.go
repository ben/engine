// +build darwin linux openbsd windows

package gfx

import (
	"sync/atomic"

	"git.lubar.me/ben/engine/log"
	"github.com/chai2010/webp"
	"golang.org/x/mobile/gl"
)

func (s *Sheet) finishFetch(b []byte) {
	img, err := webp.DecodeRGBA(b)
	if err != nil {
		log.Warningf("decoding texture sheet %q: %+v", s.Name(), err)

		lock.Lock()
		atomic.StoreUint32(&s.state, stateError)
		cond.Broadcast()
		lock.Unlock()

		return
	}

	lock.Lock()
	for glctx == nil {
		cond.Wait()
	}

	s.tex = glctx.CreateTexture()
	glctx.BindTexture(gl.TEXTURE_2D, s.tex)
	glctx.TexImage2D(gl.TEXTURE_2D, 0, gl.RGBA, img.Rect.Dx(), img.Rect.Dy(), gl.RGBA, gl.UNSIGNED_BYTE, img.Pix)
	glctx.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE)
	glctx.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE)
	glctx.GenerateMipmap(gl.TEXTURE_2D)
	atomic.StoreUint32(&s.state, stateReady)
	cond.Broadcast()
	lock.Unlock()
}
