package gfx

import (
	"sync/atomic"

	"git.lubar.me/ben/engine/internal"
	"git.lubar.me/ben/engine/log"
	"golang.org/x/mobile/gl"
)

type Sprite struct {
	_ internal.NoEqual
	internal.Named
	Sheet  *Sheet
	X0, X1 float32
	Y0, Y1 float32
	S0, S1 float32
	T0, T1 float32
}

const (
	stateInitial  = 0
	stateFetching = 1
	stateReady    = 2
	stateError    = 3
	stateReleased = 4
)

type Sheet struct {
	_ internal.NoEqual
	internal.Named
	Width     int
	Height    int
	unitPerPx float32
	tex       gl.Texture
	state     uint32
	embedded  []byte
}

func NewSheet(name string, width, height int, unitPerPx float32) *Sheet {
	return &Sheet{
		Named:     internal.Name(name),
		Width:     width,
		Height:    height,
		unitPerPx: unitPerPx,
	}
}

func NewEmbeddedSheet(name string, width, height int, unitPerPx float32, data []byte) *Sheet {
	s := &Sheet{
		Named:     internal.Name(name),
		Width:     width,
		Height:    height,
		unitPerPx: unitPerPx,
		embedded:  data,
	}

	s.StartPreload()

	return s
}

func (s *Sheet) Sprite(x, y, w, h, px, py float32) *Sprite {
	anchorX := w * px
	anchorY := h * py

	return &Sprite{
		X0: -anchorX / s.unitPerPx,
		Y0: -anchorY / s.unitPerPx,
		X1: (w - anchorX) / s.unitPerPx,
		Y1: (h - anchorY) / s.unitPerPx,
		S0: x / float32(s.Width),
		T0: 1 - (y+h)/float32(s.Height),
		S1: (x + w) / float32(s.Width),
		T1: 1 - y/float32(s.Height),

		Sheet: s,
	}
}

func (s *Sprite) Crop(left, top, right, bottom float32) *Sprite {
	return &Sprite{
		X0: s.X0*(1-left) + s.X1*left,
		Y0: s.Y0*(1-bottom) + s.Y1*bottom,
		X1: s.X1*(1-right) + s.X0*right,
		Y1: s.Y1*(1-top) + s.Y0*top,
		S0: s.S0*(1-left) + s.S1*left,
		T0: s.T0*(1-top) + s.T1*top,
		S1: s.S1*(1-right) + s.S0*right,
		T1: s.T1*(1-bottom) + s.T0*bottom,

		Sheet: s.Sheet,
	}
}

func (s *Sheet) lazyTex() gl.Texture {
	if s == nil {
		return blankTex
	}

	switch atomic.LoadUint32(&s.state) {
	case stateReady:
		return s.tex
	case stateInitial:
		log.Warningf("non-preloaded sprite sheet: %q", s.Name())

		fallthrough
	default:
		s.maybeFetch()

		return blankTex
	}
}

func (s *Sheet) maybeFetch() {
	switch state := atomic.LoadUint32(&s.state); state {
	case stateReady, stateFetching:
		// nothing to do
	case stateInitial, stateReleased:
		if atomic.CompareAndSwapUint32(&s.state, state, stateFetching) {
			go s.doFetch()
		}
	case stateError:
		// TODO: retry loading?
	}
}

func (s *Sheet) doFetch() {
	if s.embedded != nil {
		s.finishFetch(s.embedded)

		return
	}

	panic("TODO: Sheet.doFetch for non-embedded textures") // TODO
}

func (s *Sheet) StartPreload() {
	s.maybeFetch()
}
