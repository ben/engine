package gfx

type Color struct {
	R, G, B, A uint8
}

func BlackAlpha(a uint8) Color {
	return Color{A: a}
}

func WhiteAlpha(a uint8) Color {
	return Color{R: a, G: a, B: a, A: a}
}

var (
	Black Color = BlackAlpha(255)
	White Color = WhiteAlpha(255)
)
