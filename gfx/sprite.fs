#extension GL_OES_standard_derivatives : require

#ifdef GL_FRAGMENT_PRECISION_HIGH
precision highp float;
#else
precision mediump float;
#endif

uniform sampler2D tex[8];
uniform float time;

// x, y, z, w
varying vec4 position;
// r, g, b, a
varying vec4 color;

// xy: texcoord
// z: texid
// w: distance
varying vec4 packed_0;

// x: no_discard
// y: is_mtsdf
// z: is_type2 (for MTSDF, use smooth border rendering rather than sharp main rendering)
// w: (unused)
varying vec4 packed_1;

vec4 texture2DArr(int texID, vec2 coord) {
	if (texID < 4) {
		if (texID < 2) {
			if (texID < 1) {
				return texture2D(tex[0], coord);
			} else {
				return texture2D(tex[1], coord);
			}
		} else {
			if (texID < 3) {
				return texture2D(tex[2], coord);
			} else {
				return texture2D(tex[3], coord);
			}
		}
	} else {
		if (texID < 6) {
			if (texID < 5) {
				return texture2D(tex[4], coord);
			} else {
				return texture2D(tex[5], coord);
			}
		} else {
			if (texID < 7) {
				return texture2D(tex[6], coord);
			} else {
				return texture2D(tex[7], coord);
			}
		}
	}
}

float median3(vec3 v) {
	if (v.x > v.y) { v.xy = v.yx; }
	if (v.y > v.z) { v.yz = v.zy; }
	if (v.x > v.y) { v.xy = v.yx; }
	return v.y;
}

void main() {
	vec4 tex_color = texture2DArr(int(floor(packed_0.z)), packed_0.xy);

	if (packed_1.y > 0.5) {
		// antialiasing from Andrew Cassidy's write-up:
		// https://drewcassidy.me/2020/06/26/sdf-antialiasing/

		float dist;
		if (packed_1.z > 0.5) {
			dist = 0.25 - tex_color.a;
		} else {
			dist = 0.5 - median3(tex_color.rgb);
		}

		vec2 ddist = vec2(dFdx(dist), dFdy(dist));

		float pixelDist = dist / length(ddist);

		if (pixelDist > 0.5) {
			discard;
		}

		tex_color = vec4(clamp(0.5 - pixelDist, 0.0, 1.0));
	}

	if (tex_color.a < 0.5 && packed_1.x < 0.5) {
		discard;
	}

	if (packed_1.w > 0.5) {
		tex_color += time;
	}

	gl_FragColor = tex_color * color;
}
