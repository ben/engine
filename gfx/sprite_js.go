// +build js

package gfx

import (
	"sync/atomic"
	"syscall/js"

	"git.lubar.me/ben/engine/internal"
	"git.lubar.me/ben/engine/log"
	"golang.org/x/mobile/gl"
)

func (s *Sheet) finishFetch(b []byte) {
	u8 := js.Global().Get("Uint8Array").New(len(b))
	js.CopyBytesToJS(u8, b)

	blob := js.Global().Get("Blob").New([]interface{}{u8}, map[string]interface{}{
		"type": "image/webp",
	})

	url := js.Global().Get("URL").Call("createObjectURL", blob)
	defer js.Global().Get("URL").Call("revokeObjectURL", blob)

	img := js.Global().Get("Image").New()
	img.Set("width", s.Width)
	img.Set("height", s.Height)
	promise := js.Global().Get("Promise").New(js.Global().Get("Function").New("img,url", `return function finishFetch(resolve, reject) {
	img.onload = resolve;
	img.onerror = reject;
	img.src = url;
}`).Invoke(img, url))

	_, err := internal.Await(promise)
	if err != nil {
		log.Warningf("decoding texture sheet %q: %+v", s.Name(), err)

		lock.Lock()
		atomic.StoreUint32(&s.state, stateError)
		cond.Broadcast()
		lock.Unlock()

		return
	}

	lock.Lock()
	for glctx == nil {
		cond.Wait()
	}

	s.tex = glctx.CreateTexture()
	glctx.BindTexture(gl.TEXTURE_2D, s.tex)
	glctx.(gl.JSWrapper).JSValue().Call("texImage2D", gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, img)
	glctx.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE)
	glctx.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE)
	glctx.GenerateMipmap(gl.TEXTURE_2D)
	atomic.StoreUint32(&s.state, stateReady)
	cond.Broadcast()
	lock.Unlock()
}
