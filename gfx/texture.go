package gfx

import (
	"git.lubar.me/ben/engine/internal"
	"golang.org/x/mobile/gl"
)

var blankTex gl.Texture

func initBlankTex() {
	blankTex = glctx.CreateTexture()
	Blank.Sheet.tex = blankTex

	glctx.BindTexture(gl.TEXTURE_2D, blankTex)
	glctx.TexImage2D(gl.TEXTURE_2D, 0, gl.RGBA, 1, 1, gl.RGBA, gl.UNSIGNED_BYTE, []byte{255, 255, 255, 255})
	glctx.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST)
	glctx.TexParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST)
}

var Blank = &Sprite{
	Named: internal.Name("Blank"),
	Sheet: &Sheet{
		Named:     internal.Name("Blank"),
		Width:     1,
		Height:    1,
		unitPerPx: 1,
		tex:       blankTex,
		state:     stateReady,
	},
	X0: -1,
	X1: 1,
	Y0: -1,
	Y1: 1,
	S0: 0,
	S1: 1,
	T0: 0,
	T1: 1,
}
