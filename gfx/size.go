package gfx

import "golang.org/x/mobile/event/size"

var sz size.Event

func Size() size.Event {
	lock.Lock()
	e := sz
	lock.Unlock()

	return e
}

func OnSize(e size.Event) {
	lock.Lock()
	sz = e

	if glctx != nil {
		glctx.Viewport(0, 0, sz.WidthPx, sz.HeightPx)
	}
	lock.Unlock()
}
