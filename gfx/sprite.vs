// https://oeis.org/A000796
#define PI 3.14159265358979323846264338327950288419716939937510582097494459

uniform mat4 camera;
uniform mat4 perspective;

// x0, x1, y0, y1
attribute vec4 in_geom;
// s0, s1, t0, t1
attribute vec4 in_texcoord;
// x, y, z
attribute vec3 in_pos;
// rx, ry, rz
attribute vec3 in_rot;
// r, g, b, a
attribute vec4 in_color;
// textureID, flags[0..7], flags[8..15]
attribute vec3 in_texflags;
// sharedX, sharedY
attribute vec2 in_shared;

// x, y, z, w
varying vec4 position;
// r, g, b, a
varying vec4 color;

// xy: texcoord
// z: texid
// w: distance
varying vec4 packed_0;

// x: no_discard
// y: is_mtsdf
// z: is_type2 (for MTSDF, use smooth border rendering rather than sharp main rendering)
// w: (unused)
varying vec4 packed_1;

bool bit(float flags, float index) {
	return mod(floor(flags / pow(2.0, index)), 2.0) > 0.5;
}

void main() {
	vec4 sharedcoord = in_shared.xyxy;
	sharedcoord.w = 1.0 - sharedcoord.w;

	packed_1.x = bit(in_texflags.y, 0.0) ? 1.0 : 0.0;
	bool billboard = bit(in_texflags.y, 1.0);
	packed_1.y = bit(in_texflags.y, 2.0) ? 1.0 : 0.0;
	packed_1.z = bit(in_texflags.y, 3.0) ? 1.0 : 0.0;

	color = in_color;
	packed_0.z = float(in_texflags.x) + 0.5;
	packed_0.xy = mix(in_texcoord.xz, in_texcoord.yw, sharedcoord.zw);

	position = vec4(mix(in_geom.xz, in_geom.yw, sharedcoord.xy), 0.0, 1.0);

	vec3 rot = in_rot * PI * 2.0;
	
	if (billboard) {
		vec4 relative_base_position = vec4(in_pos, 1.0) * camera;
		rot.x += -atan(-relative_base_position.y, -relative_base_position.z);
		rot.y += atan(-relative_base_position.x, relative_base_position.z);
	}

	position *= mat4(
		cos(rot.z), sin(rot.z), 0.0, 0.0,
		-sin(rot.z), cos(rot.z), 0.0, 0.0,
		0.0, 0.0, 1.0, 0.0,
		0.0, 0.0, 0.0, 1.0
	);
	position *= mat4(
		1.0, 0.0, 0.0, 0.0,
		0.0, cos(rot.x), sin(rot.x), 0.0,
		0.0, -sin(rot.x), cos(rot.x), 0.0,
		0.0, 0.0, 0.0, 1.0
	);
	position *= mat4(
		cos(rot.y), 0.0, -sin(rot.y), 0.0,
		0.0, 1.0, 0.0, 0.0,
		sin(rot.y), 0.0, cos(rot.y), 0.0,
		0.0, 0.0, 0.0, 1.0
	);

	position.xyz += in_pos;
	position *= camera;
	position = perspective * position;
	packed_0.w = length(position.xyz);
	gl_Position = position;
}
