package gfx

import (
	"fmt"
	"strings"

	"golang.org/x/mobile/gl"
)

type ErrShaderLink struct {
	Vert string
	Frag string
	Link string

	Extra string
}

func (err ErrShaderLink) Error() string {
	return fmt.Sprintf("gfx: failed to compile shader:\n\nvertex:\n%s\n\nfragment:\n%s\n\nlink:\n%s%s", err.Vert, err.Frag, err.Link, err.Extra)
}

func compileShader(vertex, fragment string) (gl.Program, error) {
	vs := glctx.CreateShader(gl.VERTEX_SHADER)
	defer glctx.DeleteShader(vs)

	fs := glctx.CreateShader(gl.FRAGMENT_SHADER)
	defer glctx.DeleteShader(fs)

	prog := glctx.CreateProgram()

	glctx.ShaderSource(vs, vertex)
	glctx.ShaderSource(fs, fragment)

	glctx.CompileShader(vs)
	glctx.CompileShader(fs)

	glctx.AttachShader(prog, vs)
	glctx.AttachShader(prog, fs)

	glctx.LinkProgram(prog)

	if glctx.GetProgrami(prog, gl.LINK_STATUS) != gl.TRUE {
		fixNul := strings.NewReplacer("\x00", "␀")
		vert := fixNul.Replace(glctx.GetShaderInfoLog(vs))
		frag := fixNul.Replace(glctx.GetShaderInfoLog(fs))
		link := fixNul.Replace(glctx.GetProgramInfoLog(prog))

		glctx.DeleteProgram(prog)

		return gl.Program{}, &ErrShaderLink{
			Vert: vert,
			Frag: frag,
			Link: link,

			Extra: gatherRendererInfo(),
		}
	}

	return prog, nil
}
