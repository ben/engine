package gfx

import (
	"math"
	"reflect"
	"runtime"
	"time"
	"unsafe"

	"git.lubar.me/ben/engine/internal"
	"git.lubar.me/ben/engine/log"
	"golang.org/x/mobile/exp/f32"
	"golang.org/x/mobile/gl"
)

const maxSheetsPerBatch = 8

type RenderFlags uint16

const (
	// FlagNoDiscard prevents samples with an alpha channel below 50% from
	// being discarded.
	FlagNoDiscard RenderFlags = 1 << 0
	// FlagBillboard makes the sprite orient itself towards the camera.
	FlagBillboard RenderFlags = 1 << 1
	// FlagMTSDF causes the texture to be interpreted as a multi-channel
	// transparent signed distance field.
	FlagMTSDF RenderFlags = 1 << 2
	// FlagType2 uses an alternate rendering path for the sprite. What this
	// actually means depends on other flags. Currently, the following flags
	// have defined alternate rendering paths:
	//
	// MTSDF: use the alpha channel to render a smooth border (as opposed
	// to the sharper main shape rendering)
	FlagType2 RenderFlags = 1 << 3
)

type Batch struct {
	_      internal.NoEqual
	sheets [maxSheetsPerBatch]*Sheet
	data   []spriteData
	vao    gl.VertexArray
	vbo    gl.Buffer
	dirty  bool
}

var spriteDataShared = [4][2]uint8{{0, 1}, {0, 0}, {1, 0}, {1, 1}}

type spriteData struct {
	_          internal.NoEqual
	X0, X1     float32
	Y0, Y1     float32
	S0, S1     float32
	T0, T1     float32
	X, Y, Z    float32
	Rx, Ry, Rz uint8
	Tint       Color
	TextureID  uint8
	Flags      RenderFlags
	Shared     [2]uint8
	_          [8]uint8
}

// ensure size matches expectation
var _ [64]struct{} = [unsafe.Sizeof(spriteData{})]struct{}{}

func (b *Batch) Append(s *Sprite, x, y, z, sx, sy float32, tint Color) {
	b.AppendEx(s, x, y, z, sx, sy, tint, 0, 0, 0, 0)
}

func (b *Batch) AppendEx(s *Sprite, x, y, z, sx, sy float32, tint Color, flags RenderFlags, rx, ry, rz float32) {
	b.Reserve(1)

	texIndex := -1
	for i, sheet := range b.sheets {
		if sheet == nil || sheet == s.Sheet {
			b.sheets[i] = s.Sheet
			texIndex = i

			break
		}
	}

	if texIndex == -1 {
		for i, sheet := range b.sheets {
			log.Debugf("sheet %d: %q", i, sheet.Name())
		}

		log.Debugf("sheet -1: %q", s.Sheet.Name())

		panic("gfx: too many sprite sheets in batch")
	}

	i := len(b.data)
	b.data = b.data[:i+4]

	b.data[i].X0, b.data[i].X1 = s.X0*sx, s.X1*sx
	b.data[i].Y0, b.data[i].Y1 = s.Y0*sy, s.Y1*sy
	b.data[i].S0, b.data[i].S1 = s.S0, s.S1
	b.data[i].T0, b.data[i].T1 = s.T0, s.T1
	b.data[i].X, b.data[i].Y, b.data[i].Z = x, y, z
	b.data[i].Rx, b.data[i].Ry, b.data[i].Rz = angleMod(rx), angleMod(ry), angleMod(rz)
	b.data[i].TextureID = uint8(texIndex)
	b.data[i].Tint = tint
	b.data[i].Flags = flags

	b.data[i+1] = b.data[i]
	b.data[i+2] = b.data[i]
	b.data[i+3] = b.data[i]

	b.data[i].Shared = spriteDataShared[0]
	b.data[i+1].Shared = spriteDataShared[1]
	b.data[i+2].Shared = spriteDataShared[2]
	b.data[i+3].Shared = spriteDataShared[3]

	b.dirty = true
}

func (b *Batch) Reserve(count int) {
	total := len(b.data) + 4*count

	if total > cap(b.data) {
		b.data = append(b.data, make([]spriteData, 4*count)...)[:len(b.data)]
	}

	ensureElement(uint32(total))
}

func angleMod(a float32) uint8 {
	return uint8(math.Mod(float64(a), math.Pi*2) / (math.Pi * 2))
}

func (b *Batch) Reset() {
	for i := range b.sheets {
		b.sheets[i] = nil
	}

	b.data = b.data[:0]
	b.dirty = true
}

func sliceMatrix(mat *f32.Mat4) []float32 {
	return (*[16]float32)(unsafe.Pointer(mat))[:]
}

var startTime = time.Now()

func glTime() float32 {
	return float32(time.Since(startTime).Seconds())
}

func (b *Batch) Render(cam *Camera) {
	lock.Lock()

	if b.dirty || b.vbo == (gl.Buffer{}) {
		b.bufferData()
	}

	glctx.UseProgram(shaderProg)
	glctx.UniformMatrix4fv(uniformCamera, sliceMatrix(cam.Combined()))
	glctx.UniformMatrix4fv(uniformPerspective, sliceMatrix(&cam.Perspective))
	glctx.Uniform1f(uniformTime, glTime())

	for i, s := range b.sheets {
		glctx.ActiveTexture(gl.TEXTURE0 + gl.Enum(i))
		glctx.BindTexture(gl.TEXTURE_2D, s.lazyTex())
	}

	if haveVAO {
		glctx.BindVertexArray(b.vao)
	} else {
		glctx.BindBuffer(gl.ARRAY_BUFFER, b.vbo)
		b.setVertexAttribs()
	}

	glctx.BindBuffer(gl.ELEMENT_ARRAY_BUFFER, getElementBuffer())
	glctx.DrawElements(gl.TRIANGLES, len(b.data)/4*6, gl.UNSIGNED_INT, 0)

	lock.Unlock()
}

func (b *Batch) Release() {
	if haveVAO {
		glctx.DeleteVertexArray(b.vao)
	}

	glctx.DeleteBuffer(b.vbo)

	b.dirty = true

	runtime.SetFinalizer(b, nil)
}

func (b *Batch) bufferData() {
	if b.vbo == (gl.Buffer{}) {
		runtime.SetFinalizer(b, (*Batch).Release)

		if haveVAO {
			b.vao = glctx.CreateVertexArray()
		}

		b.vbo = glctx.CreateBuffer()
	}

	glctx.UseProgram(shaderProg)

	if haveVAO {
		glctx.BindVertexArray(b.vao)
	}

	glctx.BindBuffer(gl.ARRAY_BUFFER, b.vbo)

	var buf []byte

	if len(b.data) != 0 {
		bufHeader := (*reflect.SliceHeader)(unsafe.Pointer(&buf))
		bufHeader.Data = uintptr(unsafe.Pointer(&b.data[0]))
		bufHeader.Len = len(b.data) * int(unsafe.Sizeof(b.data[0]))
		bufHeader.Cap = cap(b.data) * int(unsafe.Sizeof(b.data[0]))
	}

	glctx.BufferData(gl.ARRAY_BUFFER, buf, gl.DYNAMIC_DRAW)

	if haveVAO {
		b.setVertexAttribs()
	}

	b.dirty = false
}

func (b *Batch) setVertexAttribs() {
	const stride = int(unsafe.Sizeof(b.data[0]))

	glctx.EnableVertexAttribArray(attribGeom)
	glctx.EnableVertexAttribArray(attribTexCoord)
	glctx.EnableVertexAttribArray(attribPos)
	glctx.EnableVertexAttribArray(attribRot)
	glctx.EnableVertexAttribArray(attribColor)
	glctx.EnableVertexAttribArray(attribTexFlags)
	glctx.EnableVertexAttribArray(attribShared)

	glctx.VertexAttribPointer(attribGeom, 4, gl.FLOAT, false, stride, int(unsafe.Offsetof(b.data[0].X0)))
	glctx.VertexAttribPointer(attribTexCoord, 4, gl.FLOAT, false, stride, int(unsafe.Offsetof(b.data[0].S0)))
	glctx.VertexAttribPointer(attribPos, 3, gl.FLOAT, false, stride, int(unsafe.Offsetof(b.data[0].X)))
	glctx.VertexAttribPointer(attribRot, 3, gl.UNSIGNED_BYTE, true, stride, int(unsafe.Offsetof(b.data[0].Rx)))
	glctx.VertexAttribPointer(attribColor, 4, gl.UNSIGNED_BYTE, true, stride, int(unsafe.Offsetof(b.data[0].Tint)))
	glctx.VertexAttribPointer(attribTexFlags, 3, gl.UNSIGNED_BYTE, false, stride, int(unsafe.Offsetof(b.data[0].TextureID)))
	glctx.VertexAttribPointer(attribShared, 2, gl.UNSIGNED_BYTE, false, stride, int(unsafe.Offsetof(b.data[0].Shared)))
}
