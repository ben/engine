// +build darwin linux openbsd windows

package gfx

import "golang.org/x/mobile/gl"

func gatherRendererInfo() string {
	return "\n\nVendor: " + glctx.GetString(gl.VENDOR) + "\nRenderer: " + glctx.GetString(gl.RENDERER)
}
