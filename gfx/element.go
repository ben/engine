package gfx

import (
	"sync"
	"unsafe"

	"golang.org/x/mobile/gl"
)

var (
	elementBuffer       []byte
	elementIndex        uint32
	elementBufferDirty  bool
	elementBufferHandle gl.Buffer
	elementBufferLock   sync.Mutex
)

func ensureElement(index uint32) {
	elementBufferLock.Lock()

	if elementIndex < index {
		for elementBufferDirty = true; elementIndex < index; elementIndex += 4 {
			buf := [...]uint32{
				0, 1, 2,
				0, 2, 3,
			}

			for i := range buf {
				buf[i] += elementIndex
			}

			elementBuffer = append(elementBuffer, (*[unsafe.Sizeof(buf)]byte)(unsafe.Pointer(&buf[0]))[:]...)
		}
	}

	elementBufferLock.Unlock()
}

func getElementBuffer() gl.Buffer {
	elementBufferLock.Lock()

	if elementBufferDirty {
		elementBufferDirty = false

		uploadElementBuffer()
	}

	handle := elementBufferHandle

	elementBufferLock.Unlock()

	return handle
}

func uploadElementBuffer() {
	if elementBufferHandle == (gl.Buffer{}) {
		elementBufferHandle = glctx.CreateBuffer()
	}

	glctx.BindBuffer(gl.ELEMENT_ARRAY_BUFFER, elementBufferHandle)
	glctx.BufferData(gl.ELEMENT_ARRAY_BUFFER, elementBuffer, gl.STATIC_DRAW)
}
