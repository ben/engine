package gfx

import (
	"sync"
	"time"

	"golang.org/x/mobile/gl"
)

var (
	glctx    gl.Context
	haveVAO  bool
	lock     sync.Mutex
	cond     = sync.NewCond(&lock)
	frameAdv func()
)

func Init(ctx gl.Context, nextFrame func()) error {
	lock.Lock()
	defer lock.Unlock()

	glctx = ctx
	_, haveVAO = ctx.(gl.Context3)

	glctx.Enable(gl.BLEND)
	glctx.DepthFunc(gl.LESS)
	glctx.BlendFunc(gl.ONE, gl.ONE_MINUS_SRC_ALPHA)

	const FRAGMENT_SHADER_DERIVATIVE_HINT = 0x8B8B
	glctx.Hint(FRAGMENT_SHADER_DERIVATIVE_HINT, gl.NICEST)

	initBlankTex()

	err := initShader()
	if err != nil {
		return err
	}

	ensureElement(1024)

	frameAdv = nextFrame

	cond.Broadcast()

	return nil
}

func Raw(f func(GL gl.Context)) {
	lock.Lock()
	defer lock.Unlock()

	f(glctx)
}

func NextFrame() {
	lock.Lock()
	f := frameAdv
	lock.Unlock()

	if f != nil {
		f()
	} else {
		time.Sleep(time.Second / 60)
	}
}
