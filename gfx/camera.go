package gfx

import (
	"math"

	"golang.org/x/mobile/exp/f32"
)

type Camera struct {
	Perspective f32.Mat4
	offset      f32.Mat4
	rotation    f32.Mat4
	position    f32.Mat4
	combined    f32.Mat4
	computed    bool
}

func (c *Camera) SetDefaults() {
	c.position.Identity()
	c.rotation.Identity()
	c.rotation.Rotate(&c.rotation, 10*math.Pi/180, &f32.Vec3{1, 0, 0})
	c.offset.Identity()
	c.offset.Translate(&c.offset, 0, 2.25, -8.25)
	c.computed = false
}

func (c *Camera) Combined() *f32.Mat4 {
	if c.computed {
		return &c.combined
	}

	c.combined.Mul(&c.position, &c.rotation)
	c.combined.Mul(&c.combined, &c.offset)

	c.computed = true

	return &c.combined
}

func (c *Camera) Position() *f32.Mat4 {
	c.computed = false

	return &c.position
}

func (c *Camera) Rotation() *f32.Mat4 {
	c.computed = false

	return &c.rotation
}

func (c *Camera) Offset() *f32.Mat4 {
	c.computed = false

	return &c.offset
}

func HorizontalFOV(angle f32.Radian) f32.Radian {
	lock.Lock()
	aspect := float64(sz.HeightPx) / float64(sz.WidthPx)
	lock.Unlock()

	return f32.Radian(2 * math.Atan(math.Tan(float64(angle)/2)*aspect))
}
