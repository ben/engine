// +build js

package gfx

import "golang.org/x/mobile/gl"

func gatherRendererInfo() string {
	s := "\n\nVendor: " + glctx.GetString(gl.VENDOR) + " (masked)\nRenderer: " + glctx.GetString(gl.RENDERER) + " (masked)"

	ext := glctx.(gl.JSWrapper).JSValue().Call("getExtension", "WEBGL_debug_renderer_info")
	if ext.Truthy() {
		s += "\nVendor: " + glctx.GetString(gl.Enum(ext.Get("UNMASKED_VENDOR_WEBGL").Int()))
		s += "\nRenderer: " + glctx.GetString(gl.Enum(ext.Get("UNMASKED_RENDERER_WEBGL").Int()))
	} else {
		s += "\nWEBGL_debug_renderer_info extension unavailable"
	}

	return s
}
