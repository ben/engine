package gfx

import (
	_ "embed"

	"golang.org/x/mobile/gl"
)

//go:embed sprite.vs
var vertexShader string

//go:embed sprite.fs
var fragmentShader string

//go:embed sprite_es3.vs
var vertexShader3 string

//go:embed sprite_es3.fs
var fragmentShader3 string

var (
	shaderProg         gl.Program
	uniformCamera      gl.Uniform
	uniformPerspective gl.Uniform
	uniformTime        gl.Uniform
	attribGeom         gl.Attrib
	attribTexCoord     gl.Attrib
	attribPos          gl.Attrib
	attribRot          gl.Attrib
	attribColor        gl.Attrib
	attribTexFlags     gl.Attrib
	attribShared       gl.Attrib
)

func initShader() error {
	var err error

	if gl.Version() == "GL_ES_3_0" {
		shaderProg, err = compileShader(vertexShader3, fragmentShader3)
	} else {
		shaderProg, err = compileShader(vertexShader, fragmentShader)
	}

	if err != nil {
		return err
	}

	glctx.UseProgram(shaderProg)

	uniformCamera = glctx.GetUniformLocation(shaderProg, "camera")
	uniformPerspective = glctx.GetUniformLocation(shaderProg, "perspective")

	glctx.Uniform1i(glctx.GetUniformLocation(shaderProg, "tex[0]"), 0)
	glctx.Uniform1i(glctx.GetUniformLocation(shaderProg, "tex[1]"), 1)
	glctx.Uniform1i(glctx.GetUniformLocation(shaderProg, "tex[2]"), 2)
	glctx.Uniform1i(glctx.GetUniformLocation(shaderProg, "tex[3]"), 3)
	glctx.Uniform1i(glctx.GetUniformLocation(shaderProg, "tex[4]"), 4)
	glctx.Uniform1i(glctx.GetUniformLocation(shaderProg, "tex[5]"), 5)
	glctx.Uniform1i(glctx.GetUniformLocation(shaderProg, "tex[6]"), 6)
	glctx.Uniform1i(glctx.GetUniformLocation(shaderProg, "tex[7]"), 7)

	uniformTime = glctx.GetUniformLocation(shaderProg, "time")

	attribGeom = glctx.GetAttribLocation(shaderProg, "in_geom")
	attribTexCoord = glctx.GetAttribLocation(shaderProg, "in_texcoord")
	attribPos = glctx.GetAttribLocation(shaderProg, "in_pos")
	attribRot = glctx.GetAttribLocation(shaderProg, "in_rot")
	attribColor = glctx.GetAttribLocation(shaderProg, "in_color")
	attribTexFlags = glctx.GetAttribLocation(shaderProg, "in_texflags")
	attribShared = glctx.GetAttribLocation(shaderProg, "in_shared")

	return nil
}
