// +build js

package engine

import (
	"context"
	"math"
	"runtime"
	"strings"
	"syscall/js"

	"git.lubar.me/ben/engine/log"
	"golang.org/x/mobile/event/key"
	"golang.org/x/mobile/event/lifecycle"
	"golang.org/x/mobile/event/mouse"
	"golang.org/x/mobile/event/paint"
	"golang.org/x/mobile/event/size"
	"golang.org/x/mobile/event/touch"
	"golang.org/x/mobile/geom"
	"golang.org/x/mobile/gl"
)

var glctx gl.Context

func main(ctx context.Context, c *Config) error {
	runtime.LockOSThread()

	if c != nil {
		doc.Set("title", c.Title)
	}

	var worker gl.Worker

	glctx, worker = gl.NewContext()
	workAvailable := worker.WorkAvailable()

	go run()

	for {
		select {
		case <-workAvailable:
			worker.DoWork()
		case err := <-fatalError:
			return err
		case <-ctx.Done():
			return ctx.Err()
		}
	}
}

var (
	win    = js.Global()
	doc    = win.Get("document")
	canvas js.Value

	lifeStage            = lifecycle.StageDead
	lastDevicePixelRatio = float32(1)
)

func voidFunc(f func()) js.Func {
	return js.FuncOf(func(js.Value, []js.Value) interface{} {
		f()

		return js.Undefined()
	})
}

func eventFunc(f func(js.Value)) js.Func {
	return js.FuncOf(func(_ js.Value, args []js.Value) interface{} {
		f(args[0])

		return js.Undefined()
	})
}

func addEventListener(this js.Value, event string, listener js.Func) {
	this.Call("addEventListener", event, listener)
}

func addEventListenerNonPassive(this js.Value, event string, listener js.Func) {
	this.Call("addEventListener", event, listener, map[string]interface{}{
		"passive": false,
	})
}

func run() {
	raf := win.Get("requestPostAnimationFrame")
	if !raf.Truthy() {
		raf = win.Get("requestAnimationFrame")
	}

	nextFrameCh := make(chan struct{}, 1)
	onNextFrame := voidFunc(func() {
		nextFrameCh <- struct{}{}
	})

	nextFrame = func() {
		raf.Invoke(onNextFrame)

		<-nextFrameCh
	}

	canvas = glctx.(gl.JSWrapper).JSValue().Get("canvas")
	doc.Get("body").Call("appendChild", canvas)

	canvas.Get("style").Set("position", "fixed")
	canvas.Get("style").Set("left", "0")
	canvas.Get("style").Set("right", "0")
	canvas.Get("style").Set("top", "0")
	canvas.Get("style").Set("bottom", "0")

	handleVisibility()

	jsHandleVisibility := voidFunc(handleVisibility)

	addEventListener(doc, "visibilitychange", jsHandleVisibility)
	addEventListener(win, "focus", jsHandleVisibility)
	addEventListener(win, "blur", jsHandleVisibility)
	addEventListener(win, "unload", voidFunc(func() {
		handleLifeStage(lifecycle.StageDead)
	}))

	handleSize()

	jsHandleSize := voidFunc(handleSize)

	addEventListener(win, "resize", jsHandleSize)

	if orientation := win.Get("screen").Get("orientation"); orientation.Truthy() {
		addEventListener(orientation, "change", jsHandleSize)
	}

	addEventListener(win, "contextmenu", eventFunc(onContextMenu))
	addEventListener(win, "keydown", eventFunc(onKeyDown))
	addEventListener(win, "keyup", eventFunc(onKeyUp))
	addEventListener(win, "mousedown", eventFunc(onMouseDown))
	addEventListener(win, "mouseup", eventFunc(onMouseUp))
	addEventListener(win, "mousemove", eventFunc(onMouseMove))
	addEventListenerNonPassive(win, "wheel", eventFunc(onWheel))
	addEventListenerNonPassive(win, "touchstart", eventFunc(onTouchStart))
	addEventListenerNonPassive(win, "touchend", eventFunc(onTouchEnd))
	addEventListenerNonPassive(win, "touchmove", eventFunc(onTouchMove))
	addEventListenerNonPassive(win, "touchcancel", eventFunc(onTouchCancel))

	handleEvent(paint.Event{External: true})
}

func handleLifeStage(newStage lifecycle.Stage) {
	if lifeStage == newStage {
		return
	}

	handleEvent(lifecycle.Event{
		From:        lifeStage,
		To:          newStage,
		DrawContext: glctx,
	})

	lifeStage = newStage
}

func handleVisibility() {
	if doc.Call("hasFocus").Bool() {
		handleLifeStage(lifecycle.StageFocused)

		return
	}

	switch doc.Get("visibilityState").String() {
	case "visible":
		handleLifeStage(lifecycle.StageVisible)
	case "hidden":
		handleLifeStage(lifecycle.StageAlive)
	}
}

func handleSize() {
	or := size.OrientationUnknown

	orientation := win.Get("screen").Get("orientation")
	if orientation.Truthy() {
		switch orientation.Get("type").String() {
		case "landscape-primary", "landscape-secondary":
			or = size.OrientationLandscape
		case "portrait-primary", "portrait-secondary":
			or = size.OrientationPortrait
		}
	}

	width := geom.Pt(win.Get("innerWidth").Float())
	height := geom.Pt(win.Get("innerHeight").Float())
	ppp := float32(1)

	if dpr := win.Get("devicePixelRatio"); dpr.Truthy() {
		ppp = float32(dpr.Float())
	}

	lastDevicePixelRatio = ppp

	widthPx := int(width.Px(ppp))
	heightPx := int(height.Px(ppp))

	canvas.Set("width", widthPx)
	canvas.Set("height", heightPx)

	handleEvent(size.Event{
		WidthPx:     widthPx,
		HeightPx:    heightPx,
		WidthPt:     width,
		HeightPt:    height,
		PixelsPerPt: ppp,
		Orientation: or,
	})
}

func onContextMenu(event js.Value) {
	event.Call("preventDefault")
}

func keyModifiers(event js.Value) key.Modifiers {
	mod := key.Modifiers(0)

	if event.Get("shiftKey").Truthy() {
		mod |= key.ModShift
	}

	if event.Get("altKey").Truthy() {
		mod |= key.ModAlt
	}

	if event.Get("ctrlKey").Truthy() {
		mod |= key.ModControl
	}

	if event.Get("metaKey").Truthy() {
		mod |= key.ModMeta
	}

	return mod
}

func isFunctionKeyCode(s string) bool {
	if len(s) < 2 {
		return false
	}

	if s[0] != 'F' {
		return false
	}

	for i := 1; i < len(s); i++ {
		if s[i] < '0' || s[i] > '9' {
			return false
		}
	}

	return true
}

func isAllowedKeyCode(code string) bool {
	// don't grab OS/Browser special keys
	if strings.HasPrefix(code, "Alt") ||
		strings.HasPrefix(code, "Control") ||
		strings.HasPrefix(code, "Shift") ||
		strings.HasPrefix(code, "Meta") ||
		strings.HasPrefix(code, "OS") ||
		strings.HasPrefix(code, "Browser") ||
		strings.HasPrefix(code, "Launch") ||
		strings.HasPrefix(code, "Audio") ||
		strings.HasPrefix(code, "Media") ||
		strings.HasPrefix(code, "Volume") ||
		strings.HasPrefix(code, "Lang") ||
		strings.HasPrefix(code, "Page") ||
		strings.HasSuffix(code, "Mode") ||
		strings.HasSuffix(code, "Lock") ||
		isFunctionKeyCode(code) ||
		code == "" ||
		code == "Unidentified" ||
		code == "PrintScreen" ||
		code == "Power" ||
		code == "Pause" ||
		code == "ContextMenu" ||
		code == "Help" ||
		code == "Fn" ||
		code == "Tab" ||
		code == "Home" ||
		code == "End" {
		return false
	}

	// allow letters, numbers, and some symbols.
	if (len(code) == 4 && code[:3] == "Key" && code[3] >= 'A' && code[3] <= 'Z') ||
		(len(code) == 6 && code[:5] == "Digit" && code[5] >= '0' && code[5] <= '9') ||
		strings.HasPrefix(code, "Numpad") ||
		strings.HasPrefix(code, "Intl") ||
		strings.HasSuffix(code, "Up") ||
		strings.HasSuffix(code, "Down") ||
		strings.HasSuffix(code, "Left") ||
		strings.HasSuffix(code, "Right") {
		return true
	}

	// additional symbols and special keys listed by name:
	switch code {
	case "Escape",
		"Minus",
		"Equal",
		"Backspace",
		"Enter",
		"Semicolon",
		"Quote",
		"Backquote",
		"Backslash",
		"Comma",
		"Period",
		"Slash",
		"Space",
		"Insert",
		"Delete":
		return true
	default:
		log.Debugf("unhandled keycode %q; ignoring for safety.", code)

		return false
	}
}

func keyCode(code string) key.Code {
	switch {
	case len(code) == 4 && code[:3] == "Key" && code[3] >= 'A' && code[3] <= 'Z':
		return key.CodeA + key.Code(code[3]-'A')
	case code == "Digit0":
		return key.Code0
	case len(code) == 6 && code[:5] == "Digit" && code[5] >= '1' && code[5] <= '9':
		return key.Code1 + key.Code(code[5]-'1')
	case code == "Numpad0":
		return key.CodeKeypad0
	case len(code) == 7 && code[:6] == "Numpad" && code[6] >= '1' && code[6] <= '9':
		return key.CodeKeypad1 + key.Code(code[6]-'1')
	case len(code) == 2 && code[0] == 'F' && code[1] >= '1' && code[1] <= '9':
		return key.CodeF1 + key.Code(code[1]-'1')
	case len(code) == 3 && code[:2] == "F1" && code[2] >= '0' && code[2] <= '2':
		return key.CodeF10 + key.Code(code[2]-'0')
	case len(code) == 3 && code[:2] == "F1" && code[2] >= '3' && code[2] <= '9':
		return key.CodeF13 + key.Code(code[2]-'3')
	case len(code) == 3 && code[:2] == "F2" && code[2] >= '0' && code[2] <= '4':
		return key.CodeF20 + key.Code(code[2]-'0')
	case code == "Enter":
		return key.CodeReturnEnter
	case code == "Escape":
		return key.CodeEscape
	case code == "Backspace":
		return key.CodeDeleteBackspace
	case code == "Tab":
		return key.CodeTab
	case code == "Space":
		return key.CodeSpacebar
	case code == "Minus":
		return key.CodeHyphenMinus
	case code == "Equal":
		return key.CodeEqualSign
	case code == "BracketLeft":
		return key.CodeLeftSquareBracket
	case code == "BracketRight":
		return key.CodeRightSquareBracket
	case code == "Backslash":
		return key.CodeBackslash
	case code == "Semicolon":
		return key.CodeSemicolon
	case code == "Quote":
		return key.CodeApostrophe
	case code == "Backquote":
		return key.CodeGraveAccent
	case code == "Comma":
		return key.CodeComma
	case code == "Period":
		return key.CodeFullStop
	case code == "Slash":
		return key.CodeSlash
	case code == "CapsLock":
		return key.CodeCapsLock
	case code == "Pause":
		return key.CodePause
	case code == "Insert":
		return key.CodeInsert
	case code == "Home":
		return key.CodeHome
	case code == "PageUp":
		return key.CodePageUp
	case code == "Delete":
		return key.CodeDeleteForward
	case code == "End":
		return key.CodeEnd
	case code == "PageDown":
		return key.CodePageDown
	case code == "ArrowRight":
		return key.CodeRightArrow
	case code == "ArrowLeft":
		return key.CodeLeftArrow
	case code == "ArrowDown":
		return key.CodeDownArrow
	case code == "ArrowUp":
		return key.CodeUpArrow
	case code == "NumLock":
		return key.CodeKeypadNumLock
	case code == "NumpadDivide":
		return key.CodeKeypadSlash
	case code == "NumpadMultiply":
		return key.CodeKeypadAsterisk
	case code == "NumpadSubtract":
		return key.CodeKeypadHyphenMinus
	case code == "NumpadAdd":
		return key.CodeKeypadPlusSign
	case code == "NumpadEnter":
		return key.CodeKeypadEnter
	case code == "NumpadDecimal":
		return key.CodeKeypadFullStop
	case code == "NumpadEqual":
		return key.CodeKeypadEqualSign
	case code == "Help":
		return key.CodeHelp
	case code == "AudioVolumeMute":
		return key.CodeMute
	case code == "AudioVolumeUp":
		return key.CodeVolumeUp
	case code == "AudioVolumeDown":
		return key.CodeVolumeDown
	case code == "MetaLeft", code == "OSLeft":
		return key.CodeLeftGUI
	case code == "ControlLeft":
		return key.CodeLeftControl
	case code == "ShiftLeft":
		return key.CodeLeftShift
	case code == "AltLeft":
		return key.CodeLeftAlt
	case code == "MetaRight", code == "OSRight":
		return key.CodeRightGUI
	case code == "ControlRight":
		return key.CodeRightControl
	case code == "ShiftRight":
		return key.CodeRightShift
	case code == "AltRight":
		return key.CodeRightAlt
	case code == "Unidentified", code == "":
		return key.CodeUnknown
	default:
		log.Debugf("unhandled key code: %q", code)

		return key.CodeUnknown
	}
}

func handleKeyEvent(event js.Value, direction key.Direction) {
	if event.Get("ctrlKey").Truthy() || event.Get("altKey").Truthy() || event.Get("metaKey").Truthy() {
		return
	}

	if !isAllowedKeyCode(event.Get("code").String()) {
		return
	}

	event.Call("preventDefault")

	if event.Get("repeat").Bool() {
		direction = key.DirNone
	}

	r := rune(-1)
	if k := []rune(event.Get("key").String()); len(k) == 1 {
		r = k[0]
	}

	handleEvent(key.Event{
		Rune:      r,
		Code:      keyCode(event.Get("code").String()),
		Modifiers: keyModifiers(event),
		Direction: direction,
	})
}

func onKeyDown(event js.Value) {
	handleKeyEvent(event, key.DirPress)
}

func onKeyUp(event js.Value) {
	handleKeyEvent(event, key.DirRelease)
}

func handleMouseEvent(event js.Value, direction mouse.Direction) {
	btn := mouse.ButtonNone
	switch direction {
	case mouse.DirNone:
		// no button
	case mouse.DirStep:
		switch event.Get("deltaMode").Int() {
		case 0:
			// TODO: values are pixels
		case 1:
			// TODO: values are lines
		case 2:
			// TODO: values are pages
		}

		switch dx, dy := event.Get("deltaX").Float(), event.Get("deltaY").Float(); {
		case math.Abs(dx) > math.Abs(dy) && dx > 0:
			btn = mouse.ButtonWheelRight
		case math.Abs(dx) > math.Abs(dy):
			btn = mouse.ButtonWheelLeft
		case dy > 0:
			btn = mouse.ButtonWheelDown
		default:
			btn = mouse.ButtonWheelUp
		}
	default:
		switch event.Get("button").Int() {
		case 0:
			btn = mouse.ButtonLeft
		case 1:
			btn = mouse.ButtonMiddle
		case 2:
			btn = mouse.ButtonRight
		}
	}

	handleEvent(mouse.Event{
		X:         float32(event.Get("clientX").Float()) * lastDevicePixelRatio,
		Y:         float32(event.Get("clientY").Float()) * lastDevicePixelRatio,
		Button:    btn,
		Modifiers: keyModifiers(event),
		Direction: direction,
	})
}

func onMouseDown(event js.Value) {
	event.Call("preventDefault")

	handleMouseEvent(event, mouse.DirPress)
}

func onMouseUp(event js.Value) {
	handleMouseEvent(event, mouse.DirRelease)
}

func onMouseMove(event js.Value) {
	handleMouseEvent(event, mouse.DirNone)
}

func onWheel(event js.Value) {
	event.Call("preventDefault")

	handleMouseEvent(event, mouse.DirStep)
}

func handleTouchEvent(event js.Value, typ touch.Type) {
	event.Call("preventDefault")

	changed := event.Get("changedTouches")
	for i := 0; i < changed.Length(); i++ {
		t := changed.Index(i)

		handleEvent(touch.Event{
			X:        float32(t.Get("clientX").Float()) * lastDevicePixelRatio,
			Y:        float32(t.Get("clientY").Float()) * lastDevicePixelRatio,
			Sequence: touch.Sequence(t.Get("identifier").Float()),
			Type:     typ,
		})
	}
}

func onTouchStart(event js.Value) {
	handleTouchEvent(event, touch.TypeBegin)
}

func onTouchEnd(event js.Value) {
	handleTouchEvent(event, touch.TypeEnd)
}

func onTouchMove(event js.Value) {
	handleTouchEvent(event, touch.TypeMove)
}

func onTouchCancel(event js.Value) {
	handleTouchEvent(event, touch.TypeEnd)
}
