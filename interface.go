package engine

import (
	"context"
	"fmt"
	"sync/atomic"
	"time"

	"git.lubar.me/ben/engine/audio"
	"git.lubar.me/ben/engine/gfx"
	"git.lubar.me/ben/engine/input"
	"git.lubar.me/ben/engine/log"
)

type Config struct {
	Title    string
	UpdateHz int
}

type Interface interface {
	Resize(ctx context.Context) error
	Update(ctx context.Context, advance bool) error
	Render(ctx context.Context) error
}

var fatalError = make(chan error, 1)

func Main(ctx context.Context, i Interface, c *Config) error {
	var cancel context.CancelFunc

	ctx, cancel = context.WithCancel(ctx)
	defer cancel()

	ictx, ctx = input.NewContext(ctx)

	go runApp(ctx, i, c)

	return main(ctx, c)
}

func runApp(ctx context.Context, i Interface, c *Config) {
	haveInitAudio := false

	updateEvery := time.Second / 60
	if c != nil && c.UpdateHz > 0 {
		updateEvery = time.Second / time.Duration(c.UpdateHz)
	}

	lastW, lastH := 0, 0

	nextUpdate := time.Now()
	for {
		if atomic.LoadUint32(&flags)&(flagHaveGLContext|flagShouldRender) == flagHaveGLContext|flagShouldRender {
			outstandingFrames := 0
			now := time.Now()

			if nextUpdate.Before(now.Add(-5 * time.Second)) {
				log.Debugf("significant frame drops; skipping %v of frame updates to catch up", now.Sub(nextUpdate).Truncate(time.Millisecond))
				nextUpdate = now
			}

			if !haveInitAudio {
				haveInitAudio = true

				err := audio.Init()
				if err != nil {
					fatalError <- fmt.Errorf("engine: initializing audio subsystem: %w", err)

					return
				}
			}

			lock.Lock()
			w, h := sz.WidthPx, sz.HeightPx
			lock.Unlock()

			if lastW != w || lastH != h {
				lastW, lastH = w, h

				if err := i.Resize(ctx); err != nil {
					fatalError <- fmt.Errorf("engine: in resize: %w", err)

					return
				}
			}

			for !nextUpdate.After(now) && outstandingFrames < 20 {
				ictx.Tick()

				if err := i.Update(ctx, true); err != nil {
					fatalError <- fmt.Errorf("engine: loop logic error: %w", err)

					return
				}

				nextUpdate = nextUpdate.Add(updateEvery)
				outstandingFrames++
			}

			if err := i.Render(ctx); err != nil {
				fatalError <- fmt.Errorf("engine: render error: %w", err)

				return
			}
		}

		if haveInitAudio {
			audio.Tick()
		}

		gfx.NextFrame()

		if err := ctx.Err(); err != nil {
			fatalError <- fmt.Errorf("engine: loop cancelled: %w", err)

			return
		}
	}
}
