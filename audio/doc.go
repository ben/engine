package audio

import (
	"math"
	"sync"
	"sync/atomic"
	"time"

	"git.lubar.me/ben/engine/log"
	"golang.org/x/mobile/exp/f32"
)

const freq = 48000

var (
	lock      sync.Mutex
	cond      = sync.NewCond(&lock)
	initReady = make(chan struct{})
)

var currentMusic *Sound

const (
	fetchNone = iota
	fetchActive
	fetchReady
	fetchError
)

type Sound struct {
	Name     string
	Pitch    float32
	Volume   float32
	MaxDelay float32

	Loop  bool
	Start float32
	End   float32

	impl  soundImpl
	fetch uint32
}

func (s *Sound) PlaySound(delay, overridePitch, overrideVolume, x, y, z float32) {
	s.fetchThen(func(offset time.Duration) {
		if s.MaxDelay != 0 && float32(offset.Seconds()) > s.MaxDelay {
			log.Debugf("not playing sound %q: loading time of %v exceeded max delay", s.Name, offset)

			return
		}

		s.playSound(delay-float32(offset.Seconds()), overridePitch, overrideVolume, x, y, z)
	})
}

func (s *Sound) PlaySoundGlobal(delay, overridePitch, overrideVolume float32) {
	s.PlaySound(delay, overridePitch, overrideVolume, float32(math.NaN()), float32(math.NaN()), float32(math.NaN()))
}

func (s *Sound) PlayMusic(restart bool) {
	if currentMusic != s || restart {
		currentMusic = s

		stopMusic()

		s.fetchThen(func(time.Duration) {
			if currentMusic != s {
				log.Debugf("discarding late-loaded music %q: another song was played", s.Name)

				return
			}

			stopMusic()
			s.playMusic()
		})
	}
}

func (s *Sound) maybeFetch(warn bool) {
	switch state := atomic.LoadUint32(&s.fetch); state {
	case fetchNone:
		if warn {
			log.Warningf("non-preloaded sound %q", s.Name)
		}

		fallthrough
	case fetchError:
		if atomic.CompareAndSwapUint32(&s.fetch, state, fetchActive) {
			go s.doFetch()
		}
	case fetchActive, fetchReady:
		// nothing to do
	}
}

func (s *Sound) doFetch() {
	if s.Volume == 0 {
		s.Volume = 1
	}

	if s.Pitch == 0 {
		s.Pitch = 1
	}

	err := s.implInit()
	if err != nil {
		log.Errorf("getting audio file %q failed: %+v", s.Name, err)

		lock.Lock()
		atomic.StoreUint32(&s.fetch, fetchError)
		cond.Broadcast()
		lock.Unlock()

		return
	}

	lock.Lock()
	atomic.StoreUint32(&s.fetch, fetchReady)
	cond.Broadcast()
	lock.Unlock()
}

func (s *Sound) fetchThen(f func(time.Duration)) {
	s.maybeFetch(true)

	switch atomic.LoadUint32(&s.fetch) {
	case fetchNone:
		// impossible
		fallthrough
	case fetchActive:
		start := time.Now()

		go func() {
			lock.Lock()
			for atomic.LoadUint32(&s.fetch) == fetchActive {
				cond.Wait()
			}
			lock.Unlock()

			if atomic.LoadUint32(&s.fetch) == fetchReady {
				f(time.Since(start))
			} else {
				log.Warningf("not playing sound %q: failed to fetch", s.Name)
			}
		}()
	case fetchReady:
		f(0)
	case fetchError:
		log.Warningf("not playing sound %q: failed to fetch", s.Name)
	}
}

func (s *Sound) StartPreload() {
	s.maybeFetch(false)
}

func StopMusic() {
	currentMusic = nil

	stopMusic()
}

func StopSounds() {
	stopSounds()
}

func SetListenerPosition(x, y, z, rx, ry, rz float32) {
	sx := f32.Sin(rx)
	cx := f32.Cos(rx)
	sy := f32.Sin(ry)
	cy := f32.Cos(ry)
	sz := f32.Sin(rz)
	cz := f32.Cos(rz)

	fx := sy * cx
	fy := -sx
	fz := cy * cx

	ux := sy*sx*cz - cy*sz
	uy := cx * cz
	uz := cy*sx*cz + sy*sz

	// right would be:
	// x = sy*sx*sz+cy*cz
	// y = cx*sz
	// z = cy*sx*sz-sy*cz

	setListenerPosition(x, y, z, fx, fy, fz, ux, uy, uz)
}

func Init() error {
	err := implInit()
	if err != nil {
		return err
	}

	close(initReady)

	return nil
}

func Tick() {
	tick()
}

func PauseAll() {
	pauseAll()
}

func ResumeAll() {
	resumeAll()
}
