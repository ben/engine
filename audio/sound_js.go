// +build js

package audio

import (
	"fmt"
	"math"
	"syscall/js"

	"git.lubar.me/ben/engine/internal"
)

var (
	paused      bool
	actx        js.Value
	musicGain   js.Value
	musicFFT    js.Value
	soundsGain  js.Value
	musicSource js.Value
)

type soundImpl struct {
	buf js.Value
}

func implInit() error {
	if ac := js.Global().Get("AudioContext"); ac.Truthy() {
		actx = ac.New()
	} else if ac = js.Global().Get("webkitAudioContext"); ac.Truthy() {
		actx = ac.New()
	} else {
		return fmt.Errorf("audio: missing required API: AudioContext")
	}

	if actx.Get("currentTime").IsNaN() {
		return fmt.Errorf("audio: AudioContext API is broken: currentTime returned NaN")
	}

	caps := js.Global().Get("navigator").Get("mediaCapabilities")
	if !caps.Truthy() {
		return fmt.Errorf("audio: missing required API: navigator.mediaCapabilities")
	}

	supported, err := internal.Await(caps.Call("decodingInfo", map[string]interface{}{
		"type": "file",
		"audio": map[string]interface{}{
			"contentType": "audio/ogg;codecs=opus",
		},
	}))
	if err != nil {
		return fmt.Errorf("audio: media capabilities API returned error: %w", err)
	}

	if !supported.Get("supported").Bool() {
		return fmt.Errorf("audio: browser does not support opus decoding")
	}

	musicGain = actx.Call("createGain")
	musicGain.Call("connect", actx.Get("destination"))
	// TODO: volume
	musicFFT = actx.Call("createAnalyser")
	musicFFT.Call("connect", musicGain)
	soundsGain = actx.Call("createGain")
	soundsGain.Call("connect", actx.Get("destination"))
	// TODO: volume

	return nil
}

func (s *Sound) implInit() error {
	resp, err := internal.Await(js.Global().Call("fetch", "/audio/"+s.Name+".opus", map[string]interface{}{
		"mode":        "cors",
		"credentials": "omit",
	}))
	if err != nil {
		return fmt.Errorf("audio: failed to open %q: %w", s.Name, err)
	}

	buf, err := internal.Await(resp.Call("arrayBuffer"))
	if err != nil {
		return fmt.Errorf("audio: failed to read %q: %w", s.Name, err)
	}

	<-initReady

	abuf, err := internal.Await(actx.Call("decodeAudioData", buf))
	if err != nil {
		return fmt.Errorf("audio: failed to decode %q: %w", s.Name, err)
	}

	s.impl.buf = abuf

	return nil
}

func (s *Sound) playSound(delay, overridePitch, overrideVolume, x, y, z float32) {
	pitch := s.Pitch
	if overridePitch != 0 {
		pitch = overridePitch
	}

	src := actx.Call("createBufferSource")
	src.Set("buffer", s.impl.buf)
	src.Get("playbackRate").Set("value", pitch)

	dest := src

	volume := s.Volume
	if overrideVolume != 0 {
		volume = overrideVolume
	}

	if volume != 1 {
		gain := actx.Call("createGain")
		gain.Get("gain").Set("value", volume)
		dest.Call("connect", gain)
		dest = gain
	}

	if !math.IsNaN(float64(x)) {
		panner := actx.Call("createPanner")

		if panner.Get("positionX").Truthy() {
			panner.Get("positionX").Set("value", x)
			panner.Get("positionY").Set("value", y)
			panner.Get("positionZ").Set("value", z)
		} else {
			panner.Call("setPosition", x, y, z)
		}

		dest.Call("connect", panner)
		dest = panner
	}

	when, offset := actx.Get("currentTime").Float(), 0.0
	if delay > 0 {
		when += float64(delay)
	} else if delay < 0 {
		offset -= float64(delay)
	}

	dest.Call("connect", soundsGain)
	src.Call("start", when, offset)
}

func (s *Sound) playMusic() {
	src := actx.Call("createBufferSource")
	src.Set("buffer", s.impl.buf)
	src.Get("playbackRate").Set("value", s.Pitch)

	src.Set("loop", s.Loop)
	src.Set("loopStart", s.Start)
	src.Set("loopEnd", s.End)

	dest := src

	if s.Volume != 1 {
		gain := actx.Call("createGain")
		gain.Get("gain").Set("value", s.Volume)
		dest.Call("connect", gain)
		dest = gain
	}

	dest.Call("connect", musicFFT)
	src.Call("start")
	musicSource = src
}

func stopMusic() {
	if musicSource.Truthy() {
		musicSource.Call("stop")
		musicSource = js.Undefined()
	}
}

func stopSounds() {
	soundsGain = actx.Call("createGain")
	// TODO: volume
	soundsGain.Call("connect", actx.Get("destination"))
}

func setListenerPosition(x, y, z, fx, fy, fz, ux, uy, uz float32) {
	listener := actx.Get("listener")

	if listener.Get("positionX").Truthy() {
		listener.Get("positionX").Set("value", x)
		listener.Get("positionY").Set("value", y)
		listener.Get("positionZ").Set("value", z)
		listener.Get("forwardX").Set("value", fx)
		listener.Get("forwardY").Set("value", fy)
		listener.Get("forwardZ").Set("value", fz)
		listener.Get("upX").Set("value", ux)
		listener.Get("upY").Set("value", uy)
		listener.Get("upZ").Set("value", uz)
	} else {
		listener.Call("setPosition", x, y, z)
		listener.Call("setOrientation", fx, fy, fz, ux, uy, uz)
	}
}

func tick() {
	if !paused {
		actx.Call("resume")
	}
}

func pauseAll() {
	if !actx.Truthy() {
		return
	}

	paused = true
	actx.Call("suspend")
}

func resumeAll() {
	if !actx.Truthy() {
		return
	}

	paused = false
	actx.Call("resume")
}
