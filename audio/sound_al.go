// +build darwin linux openbsd windows

package audio

import (
	"fmt"
	"io"
	"math"
	"runtime"

	"git.lubar.me/ben/engine/log"
	"github.com/BenLubar/opus"
	"golang.org/x/mobile/asset"
	"golang.org/x/mobile/exp/audio/al"
)

var (
	silenceTickBuffer [2]al.Buffer
	musicSource       al.Source
	soundSources      []al.Source
	loopingSong       *Sound
)

type soundImpl struct {
	buffer  al.Buffer
	loopBuf al.Buffer
}

const (
	tickHz = 60

	alSourceRelative = 0x202
	alPitch          = 0x1003
	alBuffer         = 0x1009
	alSampleOffset   = 0x1025
)

func implInit() error {
	lock.Lock()
	defer lock.Unlock()

	if err := al.OpenDevice(); err != nil {
		return fmt.Errorf("audio: opening device: %w", err)
	}

	sources := al.GenSources(17)
	musicSource = sources[0]
	soundSources = sources[1:]
	musicSource.Seti(alSourceRelative, 1)

	copy(silenceTickBuffer[:], al.GenBuffers(2))

	silenceTickBuffer[0].BufferData(al.FormatMono16, make([]byte, freq/tickHz*2), freq)
	silenceTickBuffer[1].BufferData(al.FormatStereo16, make([]byte, freq/tickHz*4), freq)

	return nil
}

func (s *Sound) implInit() error {
	f, err := asset.Open("audio/" + s.Name + ".opus")
	if err != nil {
		return fmt.Errorf("audio: fetching opus file %q: %w", s.Name, err)
	}
	defer f.Close()

	b, err := io.ReadAll(f)
	if err != nil {
		return fmt.Errorf("audio: reading opus file %q: %w", s.Name, err)
	}

	pcm, channels, err := opus.DecodeOpus(b)
	if err != nil {
		return fmt.Errorf("audio: decoding opus file %q: %w", s.Name, err)
	}

	format := uint32(al.FormatMono16)
	if channels == 2 {
		format = al.FormatStereo16
	}

	<-initReady

	lock.Lock()
	defer lock.Unlock()

	s.impl.buffer = al.GenBuffers(1)[0]

	if !s.Loop {
		s.impl.buffer.BufferData(format, pcm, freq)
	} else {
		startOffset := int(s.Start*freq) * channels * 2
		startOffset -= startOffset % (freq / tickHz * channels * 2)
		if startOffset > len(pcm) {
			startOffset = 0
		}

		endOffset := int(s.End*freq) * channels * 2
		endOffset -= endOffset % (freq / tickHz * channels * 2)
		if endOffset > len(pcm) {
			endOffset = len(pcm)
		}

		if startOffset != 0 {
			s.impl.buffer.BufferData(format, pcm[:startOffset], freq)
			s.impl.loopBuf = al.GenBuffers(1)[0]
		} else {
			s.impl.loopBuf = s.impl.buffer
			s.impl.buffer = 0
		}

		s.impl.loopBuf.BufferData(format, pcm[startOffset:endOffset], freq)
	}

	runtime.SetFinalizer(s, (*Sound).release)

	return nil
}

func (s *Sound) release() {
	if s.impl.buffer.Valid() {
		al.DeleteBuffers(s.impl.buffer)
		s.impl.buffer = 0
	}

	if s.impl.loopBuf.Valid() {
		al.DeleteBuffers(s.impl.loopBuf)
		s.impl.loopBuf = 0
	}
}

func nextFreeSource() (al.Source, bool) {
	for _, source := range soundSources {
		if source.BuffersQueued() == 0 {
			return source, true
		}
	}

	return al.Source(0), false
}

func (s *Sound) playSound(delay, overridePitch, overrideVolume, x, y, z float32) {
	lock.Lock()

	source, ok := nextFreeSource()
	if !ok {
		lock.Unlock()

		log.Warningf("audio: no available channel to play sound %q", s.Name)

		return
	}

	volume := s.Volume
	if overrideVolume != 0 {
		volume = overrideVolume
	}

	source.SetGain(volume)

	pitch := s.Pitch
	if overridePitch != 0 {
		pitch = overridePitch
	}

	source.Setf(alPitch, pitch)

	if math.IsNaN(float64(x)) {
		source.SetPosition(al.Vector{})
		source.Seti(alSourceRelative, 1)
	} else {
		source.SetPosition(al.Vector{x, y, z})
		source.Seti(alSourceRelative, 0)
	}

	switch {
	default:
		source.QueueBuffers(s.impl.buffer)
		al.PlaySources(source)
	case delay > 0:
		for i := int(delay * tickHz * pitch); i > 0; i-- {
			source.QueueBuffers(silenceTickBuffer[s.impl.buffer.Channels()-1])
		}

		source.QueueBuffers(s.impl.buffer)
		al.PlaySources(source)
	case delay < 0 && int32(delay*-freq) < s.impl.buffer.Size()/2/s.impl.buffer.Channels():
		source.QueueBuffers(s.impl.buffer)
		source.Seti(alSampleOffset, int32(freq*-delay/pitch))
		al.PlaySources(source)
	}

	lock.Unlock()
}

func (s *Sound) playMusic() {
	lock.Lock()

	if !s.Loop || s.impl.buffer.Valid() {
		musicSource.QueueBuffers(s.impl.buffer)
	}

	if s.Loop {
		musicSource.QueueBuffers(s.impl.loopBuf)

		loopingSong = s
	} else {
		loopingSong = nil
	}

	al.PlaySources(musicSource)

	lock.Unlock()
}

func tick() {
	var buf [1]al.Buffer

	lock.Lock()

	for musicSource.BuffersProcessed() != 0 {
		musicSource.UnqueueBuffers(buf[:])
	}

	ls := loopingSong

	for musicSource.BuffersQueued() < 2 && ls != nil {
		musicSource.QueueBuffers(ls.impl.loopBuf)
	}

	for _, source := range soundSources {
		for source.BuffersProcessed() != 0 {
			source.UnqueueBuffers(buf[:])
		}
	}

	lock.Unlock()
}

func stopMusic() {
	lock.Lock()

	loopingSong = nil

	al.StopSources(musicSource)
	musicSource.Seti(alBuffer, 0)

	lock.Unlock()
}

func stopSounds() {
	lock.Lock()

	if len(soundSources) != 0 {
		al.StopSources(soundSources...)
	}

	for _, source := range soundSources {
		source.Seti(alBuffer, 0)
	}

	lock.Unlock()
}

func setListenerPosition(x, y, z, fx, fy, fz, ux, uy, uz float32) {
	al.SetListenerPosition(al.Vector{x, y, z})

	al.SetListenerOrientation(al.Orientation{
		Forward: al.Vector{fx, fy, fz},
		Up:      al.Vector{ux, uy, uz},
	})
}

func pauseAll() {
	lock.Lock()

	if len(soundSources) != 0 {
		al.PauseSources(musicSource)
		al.PauseSources(soundSources...)
	}

	lock.Unlock()
}

func resumeAll() {
	lock.Lock()

	if len(soundSources) != 0 {
		al.PlaySources(musicSource)
		al.PlaySources(soundSources...)
	}

	lock.Unlock()
}
