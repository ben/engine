// +build android ios

package engine

import (
	"context"

	"golang.org/x/mobile/app"
)

func main(ctx context.Context, c *Config) error {
	app.Main(appMain)

	select {
	case err := <-fatalError:
		return err
	case <-ctx.Done():
		return ctx.Err()
	default:
		return nil
	}
}

func appMain(a app.App) {
	nextFrame = func() { a.Publish() }

	for event := range a.Events() {
		handleEvent(event)
	}
}
