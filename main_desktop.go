// +build darwin,!ios linux,!android openbsd windows

package engine

import (
	"context"

	"golang.org/x/exp/shiny/driver/gldriver"
	"golang.org/x/exp/shiny/screen"
	"golang.org/x/mobile/event/lifecycle"
)

var opts = &screen.NewWindowOptions{}

func main(ctx context.Context, c *Config) error {
	if c != nil {
		opts.Title = c.Title
	}

	gldriver.Main(shinyMain)

	select {
	case err := <-fatalError:
		return err
	case <-ctx.Done():
		return ctx.Err()
	default:
		return nil
	}
}

func shinyMain(s screen.Screen) {
	win, err := s.NewWindow(opts)
	if err != nil {
		panic(err)
	}
	defer win.Release()

	nextFrame = func() { win.Publish() }

	for {
		event := win.NextEvent()

		handleEvent(event)

		if e, ok := event.(lifecycle.Event); ok && e.To == lifecycle.StageDead {
			break
		}
	}
}
