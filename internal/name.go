// +build !nodebug

package internal

type Named struct {
	name string
}

func Name(name string) Named {
	return Named{name: name}
}

func (n Named) Name() string {
	return n.name
}
