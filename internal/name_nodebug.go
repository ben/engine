// +build nodebug

package internal

type Named struct{}

func Name(name string) Named {
	return Named{}
}

func (n Named) Name() string {
	return "(optimized out)"
}
