// +build js

package internal

import "syscall/js"

func Await(promise js.Value) (js.Value, error) {
	type resultPair struct {
		v js.Value
		e error
	}

	ch := make(chan resultPair, 1)

	onResolve := js.FuncOf(func(_ js.Value, args []js.Value) interface{} {
		ch <- resultPair{v: args[0]}

		return js.Undefined()
	})
	onReject := js.FuncOf(func(_ js.Value, args []js.Value) interface{} {
		ch <- resultPair{e: js.Error{Value: args[0]}}

		return js.Undefined()
	})

	promise = js.Global().Get("Promise").Call("resolve", promise)
	promise.Call("then", onResolve, onReject)

	result := <-ch

	onResolve.Release()
	onReject.Release()

	return result.v, result.e
}
