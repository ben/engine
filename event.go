package engine

import (
	"fmt"
	"sync"
	"sync/atomic"

	"git.lubar.me/ben/engine/audio"
	"git.lubar.me/ben/engine/gfx"
	"git.lubar.me/ben/engine/input"
	"golang.org/x/mobile/event/key"
	"golang.org/x/mobile/event/lifecycle"
	"golang.org/x/mobile/event/mouse"
	"golang.org/x/mobile/event/paint"
	"golang.org/x/mobile/event/size"
	"golang.org/x/mobile/event/touch"
	"golang.org/x/mobile/gl"
)

var (
	nextFrame func()
	sz        size.Event
	ictx      *input.Context
	flags     uint32
	lock      sync.Mutex
)

const (
	flagShouldRender = 1 << iota
	flagHaveGLContext
)

func handleEvent(event interface{}) {
	switch e := event.(type) {
	case lifecycle.Event:
		if glctx, ok := e.DrawContext.(gl.Context); ok && atomic.LoadUint32(&flags)&flagHaveGLContext == 0 {
			err := gfx.Init(glctx, nextFrame)
			if err != nil {
				panic("engine: gfx.Init returned error: " + err.Error())
			}

			atomic.StoreUint32(&flags, atomic.LoadUint32(&flags)|flagHaveGLContext)
		}

		switch e.Crosses(lifecycle.StageVisible) {
		case lifecycle.CrossOn:
			atomic.StoreUint32(&flags, atomic.LoadUint32(&flags)|flagShouldRender)
			audio.ResumeAll()
		case lifecycle.CrossOff:
			atomic.StoreUint32(&flags, atomic.LoadUint32(&flags)&^flagShouldRender)
			audio.PauseAll()
		}
	case paint.Event:
		// ignore paint events
	case size.Event:
		lock.Lock()
		sz = e
		lock.Unlock()

		gfx.OnSize(e)
		ictx.OnSize(e)
	case mouse.Event:
		ictx.OnMouse(e)
	case key.Event:
		ictx.OnKey(e)
	case touch.Event:
		ictx.OnTouch(e)
	default:
		panic(fmt.Errorf("engine: unhandled event type: %T %+v", e, e))
	}
}
