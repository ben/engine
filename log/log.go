package log

func Debug(message string) {
	logf("DEBUG", "%s", message)
}

func Debugf(message string, args ...interface{}) {
	logf("DEBUG", message, args...)
}

func Info(message string) {
	logf("INFO", "%s", message)
}

func Infof(message string, args ...interface{}) {
	logf("INFO", message, args...)
}

func Warning(message string) {
	logf("WARNING", "%s", message)
}

func Warningf(message string, args ...interface{}) {
	logf("WARNING", message, args...)
}

func Error(message string) {
	logf("ERROR", "%s", message)
}

func Errorf(message string, args ...interface{}) {
	logf("ERROR", message, args...)
}

func TODO(message string) {
	logf("TODO", "%s", message)
}

func TODOf(message string, args ...interface{}) {
	logf("TODO", message, args...)
}
