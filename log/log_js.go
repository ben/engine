// +build js

package log

import (
	"fmt"
	"strings"
	"syscall/js"
)

var console = js.Global().Get("console")

func logf(level, message string, args ...interface{}) {
	method := strings.ToLower(level)
	if level == "TODO" {
		method = "error"
	}

	console.Call(method, level+": "+fmt.Sprintf(message, args...))
}
