// +build !js

package log

import (
	"fmt"
	"log"
)

func logf(level, message string, args ...interface{}) {
	log.Output(3, level+": "+fmt.Sprintf(message, args...))
}
