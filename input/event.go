package input

import (
	"golang.org/x/mobile/event/key"
	"golang.org/x/mobile/event/mouse"
	"golang.org/x/mobile/event/size"
	"golang.org/x/mobile/event/touch"
)

func (c *Context) OnSize(e size.Event)   { c.sz = e }
func (c *Context) OnKey(e key.Event)     { c.key = append(c.key, e) }
func (c *Context) OnMouse(e mouse.Event) { c.mouse = append(c.mouse, e) }
func (c *Context) OnTouch(e touch.Event) { c.touch = append(c.touch, e) }
