package input

import (
	"context"

	"golang.org/x/mobile/event/key"
	"golang.org/x/mobile/event/mouse"
	"golang.org/x/mobile/event/size"
	"golang.org/x/mobile/event/touch"
)

type Context struct {
	sz    size.Event
	key   []key.Event
	mouse []mouse.Event
	touch []touch.Event
}

type contextKey struct{}

func NewContext(ctx context.Context) (*Context, context.Context) {
	ictx := &Context{}

	return ictx, context.WithValue(ctx, contextKey{}, ictx)
}

func Get(ctx context.Context) *Context {
	ictx, ok := ctx.Value(contextKey{}).(*Context)
	if !ok {
		panic("input: missing context")
	}

	return ictx
}

func (c *Context) Tick() {
	// TODO
}
